const passwordField = document.getElementById("passwordField");
const confirmPasswordField = document.getElementById("confirmPasswordField");
const showPasswordIcon = document.getElementById("showPasswordIcon");
const showConfirmPasswordIcon = document.getElementById("showConfirmPasswordIcon");
const submitButton = document.querySelector(".btn");
const passwordError = document.getElementById("passwordError");


function togglePasswordVisibility(inputField, iconElement) {
  if (inputField.type === "password") {
    inputField.type = "text";
    iconElement.classList.remove("fa-eye");
    iconElement.classList.add("fa-eye-slash");
  } else {
    inputField.type = "password";
    iconElement.classList.remove("fa-eye-slash");
    iconElement.classList.add("fa-eye");
  }
}

showPasswordIcon.addEventListener("click", () => {
  togglePasswordVisibility(passwordField, showPasswordIcon);
});

showConfirmPasswordIcon.addEventListener("click", () => {
  togglePasswordVisibility(confirmPasswordField, showConfirmPasswordIcon);
});

submitButton.addEventListener("click", (e) => {
  e.preventDefault(); 
  const passwordValue = passwordField.value;
  const confirmPasswordValue = confirmPasswordField.value;

  if (passwordValue === confirmPasswordValue) {
    alert("You are welcome");
    passwordError.textContent = "";
  } else {
    passwordError.textContent = "Потрібно ввести однакові значення";
  }
});
